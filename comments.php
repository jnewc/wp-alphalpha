<?php if(get_comments_number()): ?>

<hr />

<div class="section-header">Comments</div>
<div id="comment-list" class="comment-list">
<?php 
	wp_list_comments(array(
		'style' => 'div'
	)); 
?>
</div>

<?php endif; ?>

<hr />

<div class="section-header">Leave a comment</div>
<div id="comment-form" class="comment-form">
<?php
	comment_form(array(
		'comment_notes_after' => ''
	));
?>
</div>

