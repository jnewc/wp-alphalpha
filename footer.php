		
		
		<div class="footer">
			<div class="footer-sidebar footer-sidebar-left">
			<?php
				dynamic_sidebar('LeftFooterSidebar');
			?>
			</div>
			<div class="footer-sidebar footer-sidebar-right">
			<?php
				dynamic_sidebar('RightFooterSidebar');
			?>
			</div>
			<div style="clear:both"></div>
		</div>
		

	</div><!-- [END] id="main" -->
	
	<!-- FOOTER SCRIPTS -->
	<script>
		// ---------- Fixes ---------- //
		$('a[href]:has(img)').css("border-width","0");
	</script>
	
</body>
</html>