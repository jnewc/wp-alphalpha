<?php

	function jn_make_link($url, $title, $id="") {
		echo "<a " . (strlen($id)?"id=$id":"") . " href=$url>$title</a>";
	}

	/* Displays a 'more' link after an excerpt */
	function the_excerpt_more($more="") {
		if(strlen($more) <= 0){
			$cn = get_comments_number();
			$more_text = "Read more" . ($cn > 0 ? " ($cn comments)" : "");
		}
			
		global $post;
		echo '<a class="post-readmore" href="'. get_permalink($post->ID) . '"> ' . $more_text . '</a>';
	}
	
	/* Display nav links */
	function the_navlinks($toprule=false){
		if(is_single()):
			if($toprule) { ?><hr /><?php }?>
			<div class="post-navlinks">
				<div style="display:inline-block;width:300px;" class="truncate">
				<?php 
					$prevpost = get_previous_post();
					if($prevpost):
						echo jn_make_link(get_permalink($prevpost->ID), $prevpost->post_title);
					else:
						echo "No previous post";
					endif;
				?>
				</div> 
				| 
				<div style="display:inline-block;width:300px" class="truncate">
				<?php
					$nextpost = get_next_post();
					if($nextpost):
						echo jn_make_link(get_permalink($nextpost->ID), $nextpost->post_title);
					else:
						echo "No next post";
					endif;
				?>
				</div>
			</div>
			<hr /><?php 
		endif;
	}
	
	/* Initialise widget areas */
	function new_widgets_init() {
		/* INDEX SIDEBAR */
		//if(is_home()):
		register_sidebar(array(
			'name' => 'IndexSidebar',
			'id' => 'widget-area-index',
			'description' => 'Index page widget area',
			'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
			'after_widget' => "</div>"
		));
		//endif;
		
		/* Footer sidebars */
		register_sidebar(array(
			'name' => 'RightFooterSidebar',
			'id'   => 'widget-right-footer',
			'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
			'after_widget' => "</div>"	
		));
		register_sidebar(array(
			'name' => 'LeftFooterSidebar',
			'id'   => 'widget-left-footer',
			'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
			'after_widget' => "</div>"	
		));
		
	}
	// Add the widget areas
	add_action( 'init', 'new_widgets_init' );

?>