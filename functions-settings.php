<?php

	function setup_admin_menus() {
		add_menu_page('Theme settings', 'Alphalpha Theme Settings', 'manage_options',  
        'tut_theme_settings', 'the_settings_page');
	}
	
	function the_settings_page() {
		
		// Get bgimage from POST var
		$bgimage = $_POST['bgimage'];
		
		
		// Update settings if necessary.
		if(isset($_POST["update_settings"])){
			if(isset($bgimage) && strlen($bgimage) > 0) { 
				update_option("alphalpha_bgimage", $bgimage);
			} else {
				update_option("alphalpha_bgimage", null);
			}
		}
		
			
		/* Header */
		screen_icon('themes'); ?> <h1 style="font-weight:normal;">Alphalpha Theme Settings</h1>  <?php
		/* Content */
		?>
		<form method="POST" action=""> 
			<p>
				<span>Background Image URL: <span>
				<input name="bgimage" type="text" style="width:360px;border-color:grey;"
				 value="<?php 
					// If the POST var is present, set that as value, otherwise load option.
					echo (strlen($bgimage)>0?$bgimage:get_option("alphalpha_bgimage")); 
				?>" />
			</p>
		
			<input type="submit" value="Save settings" class="button-primary"/>  
			
			<input type="hidden" name="update_settings" value="1" />
		</form><?php
		
		
		
		// Update settings if necessary
		if (isset($_POST["update_settings"])) {  
			
			?><div id="message" class="updated" style="padding:6px;">Settings saved</div><?php 
		
		}
		
		
		
	}
	
	
	add_action("admin_menu", "setup_admin_menus");
	
?>