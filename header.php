<!DOCTYPE html>
<html>
<head>
	<meta charset=utf-8 />
	<title><?php bloginfo('name'); ?></title>
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_url'); ?>" />
	<style>
		<?php 
		$theme_data = get_theme('Alphalpha');
		$theme_url = get_template_directory_uri();
		$opt_bgimage = get_option("alphalpha_bgimage");
		
		if($opt_bgimage && strlen($opt_bgimage) > 0) {
			?>body {
				background:url('<?php echo $theme_url . "/" . $opt_bgimage; ?>') repeat;
				font-family:CopseRegular;
			}<?php
		} ?>
	</style>
	
	
	
	<!--[if IE]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<!-- jQuery -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
	<!-- Scripts -->
	<script src="<?php echo get_template_directory_uri(); ?>/scripts/init.js"></script>
	
	<?php wp_head() ?>
</head>
<body>
	<div id="main">
		
		<div id="header" class="header">
			<div class="header-title">
				<a href="<?php bloginfo('url'); ?>">
					jnewc.net
				</a>
			</div>
			<div class="header-description"></div>
			<div id="menu" class="navmenu">
				<?php wp_nav_menu(); ?>
			</div>
			<div style="clear:both;"></div>
		</div>
		
		
		<!--<div id="outer-content" class="outer-content">-->
			