<!-- ============================== THE LOOP ============================== -->

<!-- [START LOOP] While there are posts ... -->
<?php if ( have_posts() ) : while ( have_posts() ) : ?>
<!-- Fetch post -->
<?php the_post(); ?>

<div class="post">
	<!-- Post navlinks -->
	<?php the_navlinks(); ?>
	<!-- Post title -->
	<div class="post-header">
		<div class="post-subtitle">
			<span class="post-time">
				Posted on <?php the_time('F jS \a\t h:m'); ?> by
			</span>
			<span class="post-author">
				&nbsp;&nbsp;<?php the_author_posts_link(); ?>
			</span>
		</div>
		<div class="post-title <?php 
			if(is_single()): ?> post-title-single<?php 
			else: ?> truncate<?php endif;
		?>">
			<a <?php 
				if(!is_single()): ?>href="<?php the_permalink(); ?>"<?php endif;
				if(!is_single()): ?>class="truncate"<?php endif;
			?>>
				<?php the_title(); ?>
			</a>
		</div>
	</div>
	
	<!-- Post excerpt -->
	<div class="post-content"><?php 
		if(is_single()){
			the_content();
			comments_template();
		}
		else {
			the_excerpt();
			the_excerpt_more(); 
		}
	?></div>
	<!-- Post navlinks (footer) -->
	<?php the_navlinks(true); ?>
	<?php if(is_home()): ?><hr /><?php endif; ?>
</div>


<!-- [END LOOP] ... and print something useful if there were no posts. -->
<?php endwhile; else: echo("Holy absent blog posts Batman!");  endif; ?>