<?php get_header(); ?>
<div id="content" class="content">

<!-- ============================== THE LOOP ============================== -->

<!-- Check we're on a page -->
<?php if ( is_page() ) :  ?>
<!-- Fetch page -->
<?php get_page($_GET['page_id']); ?>

<div class="post">
	<!-- Post title -->
	<h1 class="page-title"><?php the_title(); ?></h1>
	<!-- Post subtitle -->
	<div class="page-subtitle"></div>
	<!-- Post excerpt -->
	<div class="page-content">
		
		<?php 
			the_post(); // Necessary ...
			the_content();
		?>
	</div>
	
</div>


<!-- [END LOOP] ... and print something useful if there were no posts. -->
<?php else: echo("Holy absent blog page Batman!");  endif; ?>


</div>
<?php get_footer(); ?>