Alphalpha Theme for Wordpress
=============================

_Tested with Wordpress 3.3.1_

Installation
------------

Hit "get source", then "zip", then go to your Wordpress admin panel, goto "Themes", "Install Theme", then "Upload" and upload the zip. The theme will install and can be activated back on the Themes page.