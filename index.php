<?php get_header(); ?>

<div id="content" class="content">
	<div<?php if(is_home()): ?> class="content-index"<?php endif; ?>>
		<?php get_template_part('loop', 'index'); ?>
	</div>
	<?php if(is_home()): ?>
	<div id="sidebar" class="sidebar"><?php
		/*if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('IndexSidebar')) :
			echo "ERROR: failed to load sidebar";
		endif;*/
		get_sidebar();
	?></div>
	<div style="clear:both"></div>
	<?php endif; ?>
</div>


<?php get_footer(); ?>